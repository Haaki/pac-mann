#include "Creature.h"

Creature::Creature()	//	Constructor
{
    posX = 13;
    posY = 26;
    speed = .05f;
    moveX = 0.5f;
    moveY = 0;
}

Creature::Creature(int x, int y, float s)	//	Constructor (posX, posY, speed)
{
    posX = x;
    posY = y;
    speed = s;
    moveX = 0.5f;
    moveY = 0;
}
