#ifndef MODEL
#define MODEL

#include "object.hpp"
#include "gl_texvaovbo.hpp"
#include <string>

using namespace imt2531;
using namespace glm;

class Model 
{
public:
	std::vector<TexVAOVBO*> sprite;		//	Pointer to Texture, VAO, VBO
	std::vector<glm::vec3> position;	//	Position in world space
	std::vector<glm::vec3> scale;		//	Scale of model
	std::vector<glm::vec3> rotation;	//	Rotation of model
	std::vector<Object*> object;		//	List of objects in model
	imt2531::Shader* shaderID;			//	Pointer to Shader
	imt2531::Texture* textureProgramID;	//	Pointer to Texture

	Model() {}
	Model(imt2531::Shader *sID, imt2531::Texture *tProgramID, std::vector<Object*> obj, glm::vec3 pos, glm::vec3 size, glm::vec3 rot);
	void setVertices(int obj, glm::vec3 pos, glm::vec3 size, glm::vec3 rot);	//	Sets the vertices and normals of the model
};
#endif // !MODEL
