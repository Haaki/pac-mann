#include "ANN.h"
#include "Globals.h"
#include <fstream>
#include "Functions.h"

//	Get ANN inputs
std::vector<float> ANN::GetInputs()
{
	std::vector<float> inputs;

	// 0-1, Pacman
	inputs.push_back(pacmanHandler.pacman.posX / levelData.size());
	inputs.push_back(pacmanHandler.pacman.posY / levelData[0].size());

	// 2-9, Ghosts
	for (int i = 0; i < 4; i++)
	{
		int x = ghostHandler.ghosts[i].posX;
		int y = ghostHandler.ghosts[i].posY;

		switch (ghostHandler.ghosts[i].dir)
		{
		case Right:
			x += 1;
			break;
		case Left:
			x -= 1;
			break;
		case Up:
			y -= 1;
			break;
		case Down:
			y += 1;
			break;
		default:
			break;
		}

		inputs.push_back(x / levelData.size());
		inputs.push_back(y / levelData[0].size());
	}

	// 10, Fruit
	inputs.push_back(fruitHandler.fruit.active);

	// 11... , Board and Pellets
	for (int y = 0; y < levelData[0].size(); y++)
		for (int x = 0; x < levelData.size(); x++)
			inputs.push_back(levelData[x][y] / 6);

	// Please no (input debug)
	/*for (int i = 0; i < inputs.size(); i++)
		std::cout << inputs[i] << " ";*/

	return inputs;
}

//	Randomize weights for ANN
void ANN::RandomWeights(std::vector<unsigned int> size, float magnitude)
{
	int previousSize = size[0];
	for (int i = 1; i < size.size(); i++)
	{
		std::vector<std::vector<float>> matrix;
		for (int j = 0; j < previousSize; j++)
		{
			std::vector<float> line;
			for (int k = 0; k < size[i]; k++)
				line.push_back((Random() - 0.5) * magnitude);
			matrix.push_back(line);
		}
		layerMatrices.push_back(matrix);
		previousSize = size[i];
	}
	return;
}

//	Load weights from file
void ANN::loadWeights(int num)
{
	//	Find file
    std::ifstream in;
    std::string filePath = "../resources/weights";
    filePath += std::to_string(num);
    filePath += ".txt";

	//	Read from file
    in.open(filePath);
    if (in)
    {
        std::vector<std::vector<std::vector<float>>> loadMat;
        int amount = 0;
        int IDontCare;
        in >> IDontCare;
        in.ignore();
        in >> amount;
        in.ignore();
        loadMat.resize(amount);
        for (int i = 0; i < amount; i++)
        {
            int width = 0;
            in >> width;
            in.ignore();
            loadMat[i].resize(width);
            for (int j = 0; j < width; j++)
            {
                int height = 0;
                in >> height;
                in.ignore();
                loadMat[i][j].resize(height);
                for (int k = 0; k < height; k++)
                {
                    float weight = 0.0f;
                    in >> weight;
                    loadMat[i][j][k] = weight;
                }
                in.ignore();
            }
        }
        in.close();
        layerMatrices = loadMat;
    }
    else
    {
        //RandomWeights({ ann.GetInputs().size(),16,16,5 }, glfwGetTime());
    }
}

//	Save weights to file
void ANN::saveWeights(int num)
{
	//	Find/create file
    std::ofstream out;
    std::string filePath = "../resources/weights";
    filePath += std::to_string(num);
    filePath += ".txt";

	//	Write to file
    out.open(filePath);
    if (out)
    {
        out << score << std::endl;
        out << layerMatrices.size() << std::endl;
        for (int i = 0; i < layerMatrices.size(); i++)
        {
            out << layerMatrices[i].size() << std::endl;
            for (int j = 0; j < layerMatrices[i].size(); j++)
            {
                out << layerMatrices[i][j].size() << std::endl;
                for (int k = 0; k < layerMatrices[i][j].size(); k++)
                {
                    out << layerMatrices[i][j][k] << ' ';
                }
                out << std::endl;
            }
        }
        out.close();
    }
}

//	ANN decision making
Direction ANN::makeDecision()
{
	//	Initialize
    std::vector<std::vector<float>> matrix;
    matrix = { GetInputs() };
	//	Create network
    for (int i = 0; i < layerMatrices.size(); i++)
    {
        matrix = SigmaMatrixMultiplication(matrix, layerMatrices[i]);
    }

	//	Find best values
    int bestIndex = -1;
    float bestValue = -10000000.0f;
    for (int i = 0; i < matrix[0].size(); i++)
    {
        if (matrix[0][i] > bestValue)
        {
            bestIndex = i;
            bestValue = matrix[0][i];
        }
    }

	//	Randomize output if epsilon is below threshold
	float epsilon = Random();
	if (epsilon < .1)
		bestIndex = int(Random() * 4.9f);

	//	Set move and attempt to learn from it
	lastMove = Direction(bestIndex);
	Learn();
    return Direction(bestIndex);
}

//	Learn based on recent behaviour
void ANN::Learn()
{
	//	Calculate score and initialize reward matrix
	int scoreDif = lastScore - score;
	std::vector<std::vector<float>> reward;
	reward.resize(layerMatrices[layerMatrices.size() - 1][0].size());
	
	//	If standing still
	if (lastX == pacmanHandler.pacman.posX && lastY == pacmanHandler.pacman.posY)
		scoreDif = -10;

	//	Set rewards based on score-difference(if not zero) and learn rate
	for (int i = 0; i < reward.size(); i++)
		reward[i] = { (float)-scoreDif * learnRate };

	reward[lastMove][0] *= -4.0;

	//	Apply reward
	for (int i = layerMatrices.size() - 1; i >= 0; i--)
	{
		std::vector<std::vector<float>> temp;
		temp = SigmaMatrixMultiplication(layerMatrices[i], reward);
		for (int j = 0; j < layerMatrices[i][0].size(); j++)
		{
			for (int k = 0; k < layerMatrices[i].size(); k++)
				layerMatrices[i][k][j] += reward[j][0];
		}
		reward = temp;
	}

	lastScore = score;
	lastX = pacmanHandler.pacman.posX;
	lastY = pacmanHandler.pacman.posY;
}

