#pragma once

#include <vector>
#include "enum.h"

class ANN
{
	float learnRate = 0.00025;	//	How much the ann learns from each action
	int lastScore = 0;			//	Used to keep track of what the score was before a decition was made
	Direction lastMove = None;	//	The previous move pacman made
	int lastX = 0;				//	Pacman's position before the action happened
	int lastY = 0;
public: 
    std::vector<std::vector<std::vector<float>>> layerMatrices;					//	Array of matricies containing the weights between nodes

	std::vector<float> GetInputs();												//	Fetches the inputvalues of the ANN
	void RandomWeights(std::vector<unsigned int> size, float magnitude = 1);	//	Populates the weight matrices with random weights
    void loadWeights(int num);													//	loads weights from 'slot' num (resources/weights[num].txt)
    void saveWeights(int num);													//	saves weights to 'slot'
    Direction makeDecision();													//	runs inputs through the current weights to make a decision
	void Learn();																//	handles reinforcement learning
};