#ifndef FRUIT
#define FRUIT

#include "Model.h"

class Fruit
{
public:
    int posX;						//	Position of Fruit
    int posY;						//	Position of Fruit
    int sprite;						//	Sprite and variable for setting score increase
	float timer;					//	How long the fruit is active
	bool active = false;			//	Whether the fruit is active

	std::vector<Model> model;		//	List of models in fruit

    Fruit();						//	Constructor
    Fruit(int x, int y);			//	Constructor
};
#endif // !FRUIT