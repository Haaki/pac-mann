#ifndef PELLET_HANDLER
#define PELLET_HANDLER

#include "Pellet.h"
#include "Model.h"
#include <vector>

class PelletHandler
{
public:
	std::vector<Model> models;		//	List of models
	std::vector<Pellet> pellets;	//	List of pellet objects
	bool waka = true;				//	Waka waka
	int anim = 0;					//	Animation frame
	float animT = 0;				//	Animation timer

	void draw();						//	Draw
	void checkPellet(float x, float y);	//	Check whether Pacman is close to pellets
	void eat(int index);				//	Lets Pacman eat the pellet
	void reset();						//	Reset
};
#endif // !PELLET_HANDLER
