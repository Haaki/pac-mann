﻿#pragma once

#include <string>
#include <vector>
#include "SDL_mixer.h"

struct List	//	List object
{
    std::string name;	//	Name
    std::string path;	//	Path
    int position;		//	List position
    bool dynamic_load;	//	Dynamic loading of sound
};

extern std::vector<List> bgm_list;		//	List of BGMs
extern std::vector<List> effect_list;	//	List of SFX
extern std::vector<int> channels;		//	Audio channel list
extern std::vector<Mix_Chunk*> files;	//	List of files

extern Mix_Chunk* bgm;			//	BGM pointer
extern std::string currentBGM;	//	Name of currently playing BGM
extern bool dynamic_loaded;		//	Dynamic loading of sound
extern int bgm_volume;			//	BGM volume
extern int effect_volume;		//	SFX volume

bool InitAudio();							//	Initialize Audio
void SetAudioChannels(int num_channels);	//	Set amount of audio channels

bool AddBGM(const char* path, const std::string& name, bool dynamic_load);	//	Add BGM file
bool AddEffect(const char* path, const std::string& name);	//	Add SFX file

bool PlayBGM(const std::string& name, int num_loops);	//	Play BGM
int PlayEffect(const std::string& name);	//	Play SFX

bool StopBGM();						//	Stops BGM
bool StopEffects();					//	Stops SFX

bool ToggleBGMPause();				//	Pauses BGM
bool ToggleEffectPause(int channel);//	Pauses SFX in channel
bool ToggleEffectPause();			//	Pauses SFX

int SetBGMVolume(int volume);		//	Set BGM volume
int SetEffectVolume(int volume);	//	Set Effect volume

void QuitAudio();					//	Releases Audio Manager
