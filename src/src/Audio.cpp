﻿#pragma once

#include "Audio.h"	//	Check header for details
#include <iostream>

std::vector<List> bgm_list;
std::vector<List> effect_list;
std::vector<int> channels;
std::vector<Mix_Chunk*> files;

Mix_Chunk* bgm;
bool dynamic_loaded;
int bgm_volume;
int effect_volume;
std::string currentBGM;

bool InitAudio()
    {
        if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0) return false;
        bgm_volume = 128;
        effect_volume = 128;
        for (int i = 0; i < 5; i++) channels.push_back(0);
        return true;
    }

void SetAudioChannels(int num_channels)
    {
        channels.empty();
        channels.push_back(1);
        for (int i = 0; i < num_channels; i++) channels.push_back(0);
        Mix_AllocateChannels(num_channels);
    }

bool AddBGM(const char* path, const std::string& name, bool dynamic_load)
    {
        for (auto& entry : bgm_list)
            if (entry.name == name) return false;

        int file_position = -2;

        if (!dynamic_load)
        {
            files.push_back(Mix_LoadWAV(path));
            file_position = files.size() - 1;

            if (files[file_position] == nullptr)
                return false;
        }

        unsigned int size = bgm_list.size();
        bgm_list.push_back({ name, path, file_position, dynamic_load });

        if (bgm_list.size() == size)
            return false;

        return true;
    }

bool AddEffect(const char* path, const std::string& name)
    {
        for (auto& entry : effect_list)
            if (entry.name == name) return false;

        int file_position = -2;


        files.push_back(Mix_LoadWAV(path));
        file_position = files.size() - 1;

        if (files[file_position] == nullptr)
            return false;

        unsigned int size = effect_list.size();
        effect_list.push_back({ name, path, file_position, false });

        if (effect_list.size() == size)
            return false;

        return true;
    }

bool PlayBGM(const std::string& name, int num_loops = 0)
    {
        int file_positon = -1;
        List entry_found;
        for (auto& entry : bgm_list)
            if (entry.name == name)
            {
                entry_found = entry;
                file_positon = entry.position;
            }

        if (file_positon == -1)
            return false;

		if (name != currentBGM)
		{
			currentBGM = name;
			Mix_HaltChannel(0);
			channels[0] = 1;
			if (bgm && dynamic_loaded)
				Mix_FreeChunk(bgm);
			bgm = nullptr;

			if (file_positon == -2)
				bgm = Mix_LoadWAV(entry_found.path.c_str());
			else
				bgm = files[file_positon];
			dynamic_loaded = entry_found.dynamic_load;

			if (Mix_PlayChannel(0, bgm, num_loops) == -1) return false;
			return true;
		}
    }

int PlayEffect(const std::string& name)
    {
        int file_position = -2;
        for (auto& entry : effect_list)
            if (entry.name == name) file_position = entry.position;

        if (file_position == -2) return -1;

        int empty_channel = -1;
        for (size_t i = channels.size() - 1; i >= 1; i--)
        {
            if (Mix_Playing(i)) channels[i] = 1;
            else
            {
                channels[i] = 0;
                empty_channel = i;
            }
        }

        if (empty_channel == -1) return -1;

        if (Mix_PlayChannel(empty_channel, files[file_position], 0) == -1) return false;
        channels[empty_channel] = 1;
        return empty_channel;
    }

bool StopBGM()
    {
        Mix_HaltChannel(0);
        if (Mix_Playing(0)) return false;
        if (bgm && dynamic_loaded) Mix_FreeChunk(bgm);
        bgm = nullptr;
		currentBGM = "";
        return true;
    }

bool StopEffects()
    {
        for (size_t i = 1; i < channels.size(); i++)
        {
            Mix_HaltChannel(i);
            if (Mix_Playing(i)) return false;
        }
        return true;
    }

bool ToggleBGMPause()
    {
        if (Mix_Paused(0))
        {
            Mix_Resume(0);
            return true;
        }
        else
        {
            Mix_Pause(0);
            return false;
        }
    }

bool ToggleEffectPause(int channel)
    {
        if (Mix_Paused(channel))
        {
            Mix_Resume(channel);
            return true;
        }
        else
        {
            Mix_Pause(channel);
            return false;
        }
    }

bool ToggleEffectPause()
    {
        if (Mix_Paused(channels[1]))
        {
            for (size_t i = 1; i < channels.size(); i++)
                Mix_Resume(i);
            return true;
        }
        else
        {
            for (size_t i = 1; i < channels.size(); i++)
                Mix_Pause(i);
            return false;
        }
    }

int SetBGMVolume(int volume)
    {
        Mix_Volume(0, volume);
        return bgm_volume = Mix_Volume(0, -1);
    }

int SetEffectVolume(int volume)
    {
        for (size_t i = 1; i < channels.size(); i++)
            Mix_Volume(i, volume);
        return effect_volume = Mix_Volume(1, -1);
    }

void QuitAudio()
    {
        StopBGM();
        StopEffects();
        for (size_t i = 0; i < files.size(); i++)
            Mix_FreeChunk(files[i]);
    }

