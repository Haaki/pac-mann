#ifndef ENUMS
#define ENUMS
enum Direction { None, Right, Left, Up, Down };	//	Movement Directions
enum GhostState { Scatter, Chase };				//	The ghosts state

#endif // !ENUMS