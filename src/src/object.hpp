#ifndef OBJECT
#define OBJECT

#include <vector>
#include <iostream>
#include <string>
#include "GL/glew.h"

class Object 
{
public:
    std::vector <GLfloat> vertices;	//	Vertices
    std::vector <GLfloat> normals;	//	Normals
    std::vector <GLfloat> textCoord;//	Texture
    std::string mtllib;				//

	Object() {}

    Object(std::vector <GLfloat> vert, std::vector <GLfloat> norm, std::vector <GLfloat> text, std::string mtll)
    {
		vertices = vert;	//	Vertices
		normals = norm;		//	Normals
		textCoord = text;	//	Texture
		mtllib = mtll;		//
    }
};
#endif // !OBJECT
