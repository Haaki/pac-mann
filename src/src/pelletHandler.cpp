#include "pelletHandler.h"
#include "Globals.h"
#include "Audio.h"

void PelletHandler::draw()
{
	anim = (int)(animTimer*4) % 2;	//	Update animation timer

	for (int i = 0; i < pellets.size(); i++)
	{
		if (pellets[i].active)
		{
			if (pellets[i].power)	//	Power pellet animation
			{
				if (anim == 0)
					models[i].sprite[0]->setTexture(tWhite);
				else
					models[i].sprite[0]->setTexture(tGrey);
			}

			//	Draw pellet
			models[i].sprite[0]->setCamera(cameraFocus, cameraPosition);
			models[i].sprite[0]->draw();
		}
	}
}

void PelletHandler::checkPellet(float x, float y)
{
	for (int i = 0; i < pellets.size(); i++)
		if (pellets[i].active && (sqrt((pellets[i].posX - x)*(pellets[i].posX - x) + (pellets[i].posY - y) * (pellets[i].posY - y)) < 0.5f))
			eat(i);
}

void PelletHandler::eat(int index)
{
	if (!pellets[index].power)
	{
		if (waka)						//	Waka waka
			PlayEffect("pacman_waka1");	//	Waka waka
		else							//	Waka waka
			PlayEffect("pacman_waka2");	//	Waka waka

		score += 10;	//	Increase score
	}
	else	//	Power pellet
	{
		PlayEffect("pacman_powerup");	//	Play sound
		score += 50;					//	Increase score
		for (int i = 0; i < 4; i++)
			ghostHandler.scare(i);		//	Scare Ghosts
		scareTimer = scareTime - (level - 1);	//	Set timer
		ghostsEaten = 0;				//	Reset variable
	}

	//	Reduce active pellets
	pellets[index].active = false;
	levelData[pellets[index].posX][pellets[index].posY] = 0;
	pelletsActive--;
}

void PelletHandler::reset()
{
	//	Reset all pellets to be active
	for (int i = 0; i < pellets.size(); i++)
	{
		pellets[i].active = true;
		levelData[pellets[i].posX][pellets[i].posY] = (pellets[i].power)?3:2;
	}
	pelletTotal = pellets.size();
	pelletsActive = pelletTotal;
	/*
	for (int y = 0; y < levelData[0].size(); y++)
	{
		for (int x = 0; x < levelData.size(); x++)
			std::cout << levelData[x][y] << " ";

		std::cout << std::endl;
	}*/
}