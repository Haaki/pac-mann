#include "objectLoader.hpp"
#include <fstream>
#include <string>

std::vector <Object> loadObject(std::string fileName, FileExt fileExt)
{
    std::vector <Object> obj;
    std::ifstream in;
    char mtllib[100];

    switch (fileExt)
    {
    case OBJ:
        fileName.append(".obj");
        in.open(fileName);
        if (!in.is_open())
            std::cout << "Failed to open object file\n\n";
        else
        {
            std::vector<GLfloat> vertices;
            std::vector<GLfloat> normals;
            std::vector<GLfloat> texCoord;
            char dummy[100];
            in.ignore(100, '\n');
            in.ignore(100, '\n');
            in.ignore(10, ' ');
            in.getline(mtllib, 100);

            while (!in.eof())
            {
                in.getline(dummy, 100, ' ');
                if (dummy[0] == 'o' || dummy[0] == 'g')
                {
                    obj.resize(obj.size() + 1);
                    obj[obj.size() - 1].mtllib = mtllib;
                }
                else if (dummy[0] == 'v' && dummy[1] == 'n')
                {
                    //std::cout << "vn\n";
                    normals.resize(normals.size() + 3);
                    in >> normals[normals.size() - 3];
                    in >> normals[normals.size() - 2];
                    in >> normals[normals.size() - 1];
                    //std::cout << obj.normals[obj.normals.size() - 3] << ' ' << obj.normals[obj.normals.size() - 2] << ' ' << obj.normals[obj.normals.size() - 1] << std::endl;
                    
                }
                else if (dummy[0] == 'v' && dummy[1] == 't')
                {
                    texCoord.resize(texCoord.size() + 2);
                    in >> texCoord[texCoord.size() - 2];
                    in >> texCoord[texCoord.size() - 1];
                }
                else if (dummy[0] == 'v')
                {
                    //std::cout << "v\n";
                    vertices.resize(vertices.size() + 3);
                    in >> vertices[vertices.size() - 3];
                    in >> vertices[vertices.size() - 2];
                    in >> vertices[vertices.size() - 1];
                    if (vertices.size() % 100 == 0)
                    {
                        int i = 2;
                    }
                    //std::cout << obj.vertices[obj.vertices.size() - 3] << ' ' << obj.vertices[obj.vertices.size() - 2] << ' ' << obj.vertices[obj.vertices.size() - 1] << std::endl;
                }
                else if (dummy[0] == 'f')
                {
                    float v = 0, n = 0, t = 0;
                    
                    for (int i = 0; i < 3; i++)
                    {
                        in >> v;
                        in.ignore(10, '/');
                        if(texCoord.size() != 0)
                            in >> t;
                        in.ignore(10, '/');
                        in >> n;
                        v--;
                        t--;
                        n--;

                        obj[obj.size() - 1].vertices.resize(obj[obj.size() - 1].vertices.size() + 3);
                        obj[obj.size() - 1].vertices[obj[obj.size() - 1].vertices.size() - 3] = vertices[v * 3];
                        obj[obj.size() - 1].vertices[obj[obj.size() - 1].vertices.size() - 2] = vertices[v * 3 + 1];
                        obj[obj.size() - 1].vertices[obj[obj.size() - 1].vertices.size() - 1] = vertices[v * 3 + 2];

                        if (texCoord.size() != 0)
                        {
                            obj[obj.size() - 1].textCoord.resize(obj[obj.size() - 1].textCoord.size() + 2);
                            obj[obj.size() - 1].textCoord[obj[obj.size() - 1].textCoord.size() - 2] = texCoord[t * 2];
                            obj[obj.size() - 1].textCoord[obj[obj.size() - 1].textCoord.size() - 1] = texCoord[t * 2 + 1];
                        }

                        obj[obj.size() - 1].normals.resize(obj[obj.size() - 1].normals.size() + 3);
                        obj[obj.size() - 1].normals[obj[obj.size() - 1].normals.size() - 3] = normals[n * 3];
                        obj[obj.size() - 1].normals[obj[obj.size() - 1].normals.size() - 2] = normals[n * 3 + 1];
                        obj[obj.size() - 1].normals[obj[obj.size() - 1].normals.size() - 1] = normals[n * 3 + 2];
                    }
                }
                in.ignore(100, '\n');
            }
        }

        break;
    default:
        std::cout << "File format not supported\nEmpty Object returned\n\n";
        break;
    }
    return obj;
}