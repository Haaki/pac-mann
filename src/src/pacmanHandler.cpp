#include "pacmanHandler.h"
#include "Functions.h"
#include "Globals.h"
#include "Audio.h"

void PacmanHandler::Init()
{
	//	Set camera
	cameraFocus = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 14, 3);
	cameraPosition = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 18, 0);

	reset();	//	(re)Set variables
	Model model(ShaderID, tYellow, pacmanObj,	//	Create model
		glm::vec3(0, 0, 0),
		glm::vec3(0.5f, 0.5f, 0.5f),
		glm::vec3(0, 0, 0));
	models.push_back(model);
}

void PacmanHandler::Update()
{
	float camSpeed = 4.0f;	//	The speed at which the camera adjusts to a new position

	if (!pacman.dying)
	{
		move();				//	Move Pacman
		pelletHandler.checkPellet(pacman.posX + pacman.moveX, pacman.posY + pacman.moveY);	//	Check pellet position
	}

	//	Camera modes
	if (cameraMode == 0)	//	Full map view
	{
		//	Camera goals
		glm::vec3 pos = glm::vec3(0, -.1f, 32);
		glm::vec3 foc = glm::vec3(0, 0, 0);

		//	Lerp camera
		cameraPosition = cameraPosition + (pos - cameraPosition) * deltaTime * camSpeed;
		cameraFocus = cameraFocus + (foc - cameraFocus) * deltaTime * camSpeed;
	}
	else if (cameraMode == 1)	//	Track Pacman
	{
		//	Camera goals
		glm::vec3 pos = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 14, 10);
		glm::vec3 foc = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 18, 0);

		//	Lerp camera
		cameraPosition = cameraPosition + (pos - cameraPosition) * deltaTime * camSpeed;
		cameraFocus = cameraFocus + (foc - cameraFocus) * deltaTime * camSpeed;
	}
	else if (cameraMode == 2)	//	First-person
	{
		//	Find direction
		if (pacman.dir != None)
			cam = pacman.dir;
		else
			cam = Up;

		if (cam != None)
			currenOffset = camOffsets[cam - 1];
		else
			(currenOffset = camOffsets[Up - 1]);

		//	Camera goals
		glm::vec3 pos = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 18, 0) + currenOffset;
		glm::vec3 foc = glm::vec3(pacman.posX + pacman.moveX - 14, -pacman.posY - pacman.moveY + 18, 0);

		//	Lerp camera
		cameraPosition = cameraPosition + (pos - cameraPosition) * deltaTime * camSpeed;
		cameraFocus = cameraFocus + (foc - cameraFocus) * deltaTime * camSpeed;

		lastCam = cam;
	}

	draw();	//	Draw Pacman
}

void PacmanHandler::reset()
{
	pacman = Pacman(startX, startY, pacmanSpeed);	//	Reset Pacman object
}

void PacmanHandler::draw()
{
	glm::vec3 facing;
	switch (pacman.dir)
	{ 
	case Up:	facing = glm::vec3(0, 0, PI / 2 * 3);	break;
	case Left:	facing = glm::vec3(0, 0, PI);	break;
	case Right: facing = glm::vec3(0, 0, 0);	break;
	case Down:	facing = glm::vec3(0, 0, PI / 2);	break;
	default:	facing = glm::vec3(0, 0, PI / 2);	break;
	}

	if (!pacman.dying)
	{
		//	Normal animation cycle
		pacman.anim = (int)(animTimer*8) % 3;
	}
	else
	{
		//	Death animation
		animDeathTimer += deltaTime * 4;
		pacman.anim = (int)animDeathTimer;
		if (pacman.anim == 5)	//	Finished final animation frame
		{
			kill();
			return;
		}
	}

	//	Update model
	models[0].setVertices(
		pacman.anim,
		glm::vec3(-((float)(levelData.size()) / 2) + (pacman.posX + pacman.moveX), ((float)(levelData[0].size()) / 2) - (pacman.posY + pacman.moveY), 0),
		glm::vec3(0.5f, 0.5f, 0.5f),
		facing);
	models[0].sprite[pacman.anim]->setCamera(cameraFocus, cameraPosition);
	models[0].sprite[pacman.anim]->draw();
}

void PacmanHandler::move()
{
	//	Set speed
	pacman.speed = pacmanSpeed * (1 + pacman.power * .2f);

	//	Asks for new direction if there is none, allows you to reverse direction on the spot
	if (pacman.dir == None
		|| !checkDirection(pacman.dir, pacman.posX, pacman.posY, true)
		|| pacman.dir == Right && pacman.nextDir == Left
		|| pacman.dir == Left && pacman.nextDir == Right
		|| pacman.dir == Up && pacman.nextDir == Down
		|| pacman.dir == Down && pacman.nextDir == Up)
		newDirection();

	//	Move towards next tile
	if (pacman.dir == Right && checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
	{
		pacman.moveX += pacman.speed * deltaTime * 60;
		pacman.moveY = 0;
	}
	if (pacman.dir == Left && checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
	{
		pacman.moveX -= pacman.speed * deltaTime * 60;
		pacman.moveY = 0;
	}
	if (pacman.dir == Down && checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
	{
		pacman.moveX = 0;
		pacman.moveY += pacman.speed * deltaTime * 60;
	}
	if (pacman.dir == Up && checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
	{
		pacman.moveX = 0;
		pacman.moveY -= pacman.speed * deltaTime * 60;
	}

	//	Has moved to next tile
	if (pacman.dir == Right && pacman.moveX > 1)
	{
		if (pacman.posX + 1 == levelData.size())
			pacman.posX = 0;
		else if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.posX++;

		lastTurn = Right;
		if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.moveX--;
		else
			pacman.moveX = 0;
		newDirection();
	}
	if (pacman.dir == Left && pacman.moveX < -1)
	{
		if (pacman.posX - 1 == -1)
			pacman.posX = levelData.size() - 1;
		else if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.posX--;

		lastTurn = Left;
		if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.moveX++;
		else
			pacman.moveX = 0;
		newDirection();
	}
	if (pacman.dir == Down && pacman.moveY > 1)
	{
		if (pacman.posY + 1 == levelData[pacman.posX].size())
			pacman.posY = 0;
		else if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.posY++;

		lastTurn = Down;
		if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.moveY--;
		else
			pacman.moveY = 0;
		newDirection();
	}
	if (pacman.dir == Up && pacman.moveY < -1)
	{
		if (pacman.posY - 1 == -1)
			pacman.posY = levelData[pacman.posX].size() - 1;
		else if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.posY--;

		lastTurn = Up;
		if (checkDirection(pacman.dir, pacman.posX, pacman.posY, true))
			pacman.moveY++;
		else
			pacman.moveY = 0;
		newDirection();
	}
}

void PacmanHandler::newDirection()
{
	//	Sets the next movement direction based on saved input
	if (pacman.nextDir == Right && checkDirection(pacman.nextDir, pacman.posX, pacman.posY, true))
		pacman.dir = Right;
	if (pacman.nextDir == Left && checkDirection(pacman.nextDir, pacman.posX, pacman.posY, true))
		pacman.dir = Left;
	if (pacman.nextDir == Down && checkDirection(pacman.nextDir, pacman.posX, pacman.posY, true))
		pacman.dir = Down;
	if (pacman.nextDir == Up && checkDirection(pacman.nextDir, pacman.posX, pacman.posY, true))
		pacman.dir = Up;
}

void PacmanHandler::die()
{
	pacman.dying = true;				//	Activate death animation
	pacman.dir = pacman.nextDir = lastInput = lastTurn = Up;	//	Set directions to up
	PlayEffect("pacman_death");			//	Play death sound
}

void PacmanHandler::kill()
{
	animDeathTimer = 0;		//	Reset timer
	pacman.dead = true;		//	Pacman is dead
	lives--;				//	Reduce lives
	if (lives <= 0)			//	If out of lives
		resetGame();		//		Reset game
	else					//	Else
		resetCreatures();	//		Reset creatures in level
}

void PacmanHandler::input(Direction d)	//	Give input relative to camera
{
	Direction t = lastTurn;
	if (d != None)
	{
		lastInput = d;

		if (cameraMode != 2)
			pacman.nextDir = d;
		else
		{
			if (t == None)
				t == Up;
			if ((t == Up && d == Right) || (t == Left && d == Down) || (t == Down && d == Left))
				pacman.nextDir = Right;
			else if ((t == Down && d == Right) || (t == Right && d == Down) || (t == Up && d == Left))
				pacman.nextDir = Left;
			else if ((t == Left && d == Right) || (t == Down && d == Down) || (t == Right && d == Left))
				pacman.nextDir = Up;
			else if ((t == Right && d == Right) || (t == Up && d == Down) || (t == Left && d == Left))
				pacman.nextDir = Down;
		}
	}
}

glm::vec2 PacmanHandler::getPos()
{
	return  glm::vec2(pacman.posX + pacman.moveX, pacman.posY + pacman.moveY);
}
