#ifndef PELLET
#define PELLET
#include "Model.h"
class Pellet
{
public:
    int posX;						//	Position of pellet
    int posY;						//	Position of pellet
    bool active;					//	Whether the pellet is active
    bool power;						//	Whether the pellet is a power pellet
    int anim = 0;					//	Animation frame
    float animT = 0;				//	Animation timer

    Pellet();						//	Constructor
    Pellet(int x, int y, bool p);	//	Constructor
};
#endif // !PELLET
