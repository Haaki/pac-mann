#version 330

in vec3 position;     
in vec2 textureCoordinate;
in vec3 normal;    
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat3 normalMatrix; //<-- This will be used to calculate our normals in eye space
  
out vec2 texCoord; 
out float diffuse;        

//the simplest function to calculate lighting 
float doColor()
{
    vec3 norm  = normalize(normalMatrix * normalize(normal));
    vec3 light = normalize(vec3(1.0, 1.0, 1.0));
    diffuse = max(dot(norm,light),0.0 );

    return diffuse;
}

void main(void)
{
    diffuse = doColor();
    texCoord = textureCoordinate;
    gl_Position  = projection * view * model *vec4(position,1.0);	
}